# Patcher Protocol

This page describes the patcher server protocol, which works on top of the general protocol. This is the protocol used by the `Patcher.exe` program to update files before launching.

Firstly, onverse is split up into individual component of game data referred to as "bundles". e.g.

* `onverse-client` contains all client scripts
* `onverse-data` contains 
* `onverse-win32` contains the windows executable

The actual patch bundles need to be stored on a http server. This is pointed to in the response packets.

## Basic packet Types

All of these packets are sent and received without being encapsulated in any other packet.

### TorqueRPC Types

All RPC responses should be wrapped by the equivalent RPC packet. The id of the innermost packet should reflect the state of the request. i.e. 0 means the request has been completed, whereas usually 1 will be the request data and 2 the response data.

Each RPC type will be described in terms of the communication flow.

## 420 - RequestUpdate

To start, the client should send a wrapped:

| Packet(1) | Field Type | Name | Comments |
| - | - | - | - |
| | String  | bundleName | `onverse-cache`, `onverse-data`, `onverse-scripts`, `onverse-win32` or `onverse-osx` |
| | String  | bundleVersion | version of bundle e.g. `0.9.78.4435` |

The server should take these parameters and determine if any patches are available, then send the following response packet:

| Packet(2) | Field Type | Name | Comments |
| -: | - | - | - |
| | UInt16  | numPatches | Number of patches available |
| | String  | bundleName | Should be identical to `bundleName` in request packet |

For each patch (continuing the same packet):

| Packet(2) | Field Type | Name | Comments |
| -: | - | - | - |
|  | String  | fileName | Filename of patch |
|  | String  | serverFolder | Path on server |
|  | String  | srcVersion | Version we apply against |
|  | String  | newVersion | New version we will become |
|  | UInt32BE | fileSize | Size of patch |
|  | UInt32BE | CRC | CRC32 of patch |

Finally to finish off:

| Packet(0) | Field Type | Name | Comments |
| - | - | - | - |
| | UInt32BE | responseCode | 0 for success, -1 for error |


To calculate the actual path for each path, the client should use information from the `ClientRequestMirrors` packet and the paths and filename in each patch record.

Specifically, if `serverFolder` is blank, the path should be calculated as:

`http://<hostName>/<rootPath><newVersion>/<fileName>`

Otherwise it will be calculated as:

`http://<hostName>/<rootPath><serverFolder><fileName>`

The patcher should download and apply each patch in the order specified. If a problem occurs with a patch, the user should be offered the option to download a patch file containing the complete current state of the game files (specified by using a blank version string).

## 421 - ClientRequestMirrors

To start, the client should send a wrapped :

| Packet(1) | Field Type | Name | Comments |
| - | - | - | - |
| | UInt32BE | unknown | ??? |

The server should respond with the following packet for each mirror available:

| Packet(2) | Field Type | Name | Comments |
| - | - | - | - |
| | UInt32BE | unknown | ??? |
| | UInt16(1)  | index | Index of mirror |
| | Bool  | flag | ??? |
| | String  | name | ??? |
| | String  | hostname | ??? |
| | String  | rootPath | ??? |

And finish off with:

| Packet(0) | Field Type | Name | Comments |
| - | - | - | - |
| | UInt32BE | responseCode | 0 for success, -1 for error |

## 422 - RequestExecutable

This packet is specifically to handle cases where the patcher executable needs replacing. It __does not__ relate to the onverse executable.

To start, the client should send a wrapped :

| Packet(1) | Field Type | Name | Comments |
| - | - | - | - |
| | UInt32BE | bundleName | `osx` or `win32` |
| | UInt32BE | bundleVersion | something of the form `0.9.78.4435` (should match the string inside the patcher exe) |

The server should respond with the following packet, if a newer patcher executable is available:

| Packet(2) | Field Type | Name | Comments |
| -: | - | - | - |
|  | UInt32BE  | size | Size of patcher exe |
|  | UInt32BE  | crc | CRC of patcher exe |
|  | String  | serverPath | Path on mirror |
|  | String  | exeName | Filename on mirror |

And finish off with:

| Packet(0) | Field Type | Name | Comments |
| - | - | - | - |
| | UInt32BE | responseCode | 0 for success, -1 for error |

The client will then download and launch the described patcher from the active http mirror.

# 423 - GetChannelPatchBundleSet

This is only implemented in ovopen. It's designed to facilitate generating `patch.dat` files from scratch.

The following subpackets are used:

| Sub-Packet ID | Originator | Name | Comments |
| ------------- | ------------- | ------------- | ------------- |
| 1  | Client | ChannelIdentifier | Bundle identifier |
| 2  | Server | ChannelPatch | Bundle identifier |

Return codes (packet 0):

| Code | Name |
| ------------- | ------------- |
| -2 | Error |
| -3 | Invalid channel |
| 0 | Success |

### Subpacket 1 - ChannelIdentifier

Requests a correctly formatted patch.dat for a distribution channel. 

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| String  | name | name of channel |

### Subpacket 2 - ChannelPatch

Returns data formatted in `PatchFileFormat` which can be used to determine the current active bundle versions for a channel.

If you wish to generate a clean `patch.dat` file from this (for a fresh install), the bundle versions should be reset to ''.

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| String  | channelName | Name of distribution channel e.g. `linux-server` |
| char[]  | patchData | Patch data (PatchFileFormat) |


## Patcher workflow

The patcher will first login. Then it will send `ClientRequestMirrors` RPC request. The server must then send wrapped `ClientRequestMirrors` responses containing a `UpdateMirror` packet for each mirror available, finally finishing with a standard 0 response packet.

The patcher will then send `RequestExecutable` (unless this has been disabled).

For each bundle known to the patcher, it will send `RequestUpdate` RPC request. These will contain the bundle name along with the bundle version. For clients without any data or executable files, they will specify the version as a blank string. Otherwise, they will use the last version downloaded, as described in the `patch.dat` file.

The server should send an RPC response packet containing every patch required to update to the latest version, from the earliest to the latest, finally finishing with a standard 0 response packet.

The patcher will download each patch file and attempt to apply it, updating the current bundle version numbers for each complete patch.

If no bundles are returned, the patcher will assume everything is up to date and launch the game.

## Patch bundle logistics

Assuming one wants users to keep the game up to date, up to date bundles need to be generated every time there is a change which affects the client.

If we were to keep things simple, every time the game needed updating we could just make a new full bundle of assets (e.g. type `2` bundles). This may not be desireable however if we make only minor changes (like adding a new asset or modifying a script). Thus it's advisable to generate type `1` delta patch bundles where possible.

Ideally generating the bundles should be a part of an established asset pipeline (e.g. a build script to generate `onverse-client` bundle). There should then be a secondary process which generates delta bundles from full bundles.

Clients only a few versions behind the current bundle version(s) can then download and use the delta bundles to update to the current version, while clients severely out of date (or who haven't downloaded anything at all previously) can download the latest full bundle(s).


## Patch bundle format

See the [Patch File Format](./PatchFileFormat.md) doc for information on the patch bundle and `patch.dat` format.
