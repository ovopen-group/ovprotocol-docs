# Patch File Format

The patcher file format is a custom archive format which uses lzma compression combined with delta patches. When applied, a patch file will take a bundle of files and apply them over the files present in the current working directory, giving the equivalent of if you had downloaded the desired version to start off with.

In addition, a `patch.dat` file is used to store a series of key,value pairs detailing which bundles are present or need fetching from the server. Usually it will just store patch bundles with the last version downloaded.

If a bundle is not listed in `patch.dat`, it will not be downloaded from the patch mirror.

Note that `String` values in this case are stored as follows:

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt16BE  | length | Length of string |
| char[length]  | data | String data |

## patch.dat

First we start off with a header:

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt16BE  | magic | Should be 21862 |
| UInt8  | version | Should be 1 |

Then records should be read until EOF. 

Usually the records will be in the following order:

* PatchServer (if custom server used)
* For each bundle:
	* SetBundle (set bundle)
	* BundleVersion (set bundle version)
	* For each bundle OS key:
		* SetBundleOSKey
	* For each bundle key:
		* SetBundleKey
	* EndBundle
* For each global key:
	* SetGlobalKey
* EndBundle

### Record 0 - EndBundle

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt8  | code | 0 |
| String  | version | Bundle version |

### Record 1 - PatchServer

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt8  | code | 1 |
| String  | patchServer | Last patch server used (e.g. patch.onverse.com) |

### Record 2 - SetBundle

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt8  | code | 2 |
| String  | bundleName | Name of bundle (e.g. onverse-scripts) |

### Record 3 - SetGlobalKey

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt8  | code | 3 |
| String  | key | NVP key |
| String  | value | NVP value |

### Record 0x33 - BundleVersion

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt8  | code | 0x33 |
| String  | version | Bundle version (or blank if nothing downloaded yet) |

### Record 0x34 - SetBundleOSKey

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt8  | code | 0x34 |
| String  | key | NVP key |
| String  | value | NVP value |

### Record 0x35 - SetBundleKey

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt8  | code | 0x35 |
| String  | key | NVP key |
| String  | value | NVP value |

## Patch Bundle

First the patch bundle must be decompressed in order to be manipulated. You should use standard lzma decompression for this - for example you can use `xz` command on linux to decompress a bundle.

Now for the format, we start off with a header:

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt16BE  | magic | Should be 20561 |
| UInt8  | version | Should be 2 |
| UInt8  | kind | Kind of patch file. `1` is a delta patch. `2` is just a bundle of files which will replace existing files. |
| UInt32BE  | dataStart | Start of file data (should be after `folder` data) |
| Folder | folder | root folder |
| UInt32BE | endOffset | End of data offset |

### Folder - Folder Record

Folders should be written in a nested fashion using the following format:

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt32BE  | endOffset | End of data offset (should be at the end of the folder records) |
| String  | name | Folder name (relative to parent folder) |
| UInt8  | kind | Kind of folder. `0` = NOP. `1` = PATCH. `2` = ADD. `3` = DELETE |
| UInt32BE  | numFiles | Number of files |
| FileEntry[] | files | List of files in folder |
| UInt32BE  | numFolders | Number of folders |
| Folder[] | folders | Nested list of child folders |

If making a patch of type `2`, usually all non-root folders will be of kind `ADD`. 

If making a delta patch of type `1`, usually:
* Unmodified folders are of kind `NOP`
* Modified folders (added or removed files) are of kind `PATCH`
* Added folders are of kind `ADD`
* Deleted folders are of kind `DELETE`


### FileEntry - File Record

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt8  | kind | Kind of file. Same as Folder kind. |
| String  | name | File name (relative to parent folder) |
| bool  | isLink | Is file symlink? (relevant to linux/osx only)  |

If `isLink` is true, the following additional data will be present:

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| String  | pathName | Path name of symlink |

Then the record ends.

If `kind` is not `DELETE`, the following additional data will be present:

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| bool  | isExecutable | Is file an executable? (relevant to linux/osx only)  |
| UInt32BE  | crc | Final CRC of file |
| UInt8[16]  | md5 | Final MD5 of file |
| UInt32BE  | patchCrc | CRC of file to patch *ONLY PRESENT FOR PATCH kind* |
| UInt8[16]  | patchMd5 | MD5 of file to patch *ONLY PRESENT FOR PATCH kind* |
| Win32Time  | ctime | Creation time |
| Win32Time  | mtime | Modification time |
| Win32T  | offset | Offset of file data (relative to `dataStart`) |
| UInt32BE  | size | Size of file data |

`Win32Time` is a `UInt32BE` which can be decoded as follows to posix time:

	static const uint64_t EPOCH = static_cast<uint64_t>(116444736000000000ULL);
	uint64_t ll = ((uint64_t)info.mtime) + (((uint64_t)info.ctime) << 32);
	ll = (ll - EPOCH) / 10000000ULL;
	time_t posix_time = (time_t)ll;

## Patch data

If a file is of kind `PATCH`, the file data consists of patch data, which is very similar to the `vpatch` format with unneccesary fields removed (since the patch file stores file lists instead).

`TODO: document this`.





