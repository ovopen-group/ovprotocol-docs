# Master Protocol

This page describes the master server (or "frontend") protocol, which works on top of the general protocol. This is the protocol used by the client to do most of the matchmaking functionality.

There are three variants of the master server protocol described here. One is used in pre-2012 clients, one for internal clients, and the final used in public post-2012 clients. Differences between the three will be noted as required.

## Basic packet Types

All of these packets are sent and received without being encapsulated in any other packet.

### 202 - FriendList

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt16BE  | numFriends | Number of friends|
| For each friend |
| UInt32BE  | UID | UID of friend  |
| String  | Name | Username of friend |
| UInt8  | State | State of friend |

Friends can have the following states:

| Code | State |
| ------------- | ------------- |
| 0  | Offline |
| 1  | Online |
| 2  | Requires approval |
| 3  | Pending friend request |
| 4  | Ignored |


### 203 - FriendUpdate

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt32BE  | UID | UID of friend  |
| String  | Name | Username of friend |
| UInt8  | State | State of friend |

### 204 - InstanceReady

For pre-2012 clients:

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| Int32BE  | LocationID | ID of location |
| Int32BE  | InstanceID | Instance ID of location  |
| UInt16BE  | Port | Port of location server  |

For post-2012 clients:

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| Int32BE  | LocationID | ID of location |
| Int32BE  | InstanceID | Instance ID of location  |
| UInt8[4]  | address | IP address of server  |
| UInt16BE  | Port | Port of location server  |

This packet is usually returned before the completion packet of the `RequestInstance` RPC call.

### 207 - ServerMessage

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| String  | Message | The message |

### 208 - ServerAnnouncement

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| String  | Message | The message |

### 209 - PrivateMessage

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt8  | SourceType | Source type of message |
| String  | Username | User who sent the message |
| String  | Message | The message |

`SourceType` can be one of the following:

| Code | Comments |
| ------------- | ------------- |
| 0  | Private |
| 1  | Friends |
| 2  | Guides |
| 3  | User to guide (post-2012 only) |

In the case of user to guide messages, these will not be handled correctly in pre-2012 clients.

### 210 - GuideReplyMessage

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| String  | From | User who sent the message. |
| String  | To | User intended to receive the message |
| String  | Message | The message |

This handles recording guide replies to users, which should be sent to every guide.

### 1002 - CatChildren

This is used as part of the admin interface to list categories of templates for datablock objects.

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt16BE  | FolderID | ID of folder |
| UInt16BE  | TableID | ID of object table type |
| UInt16BE  | FolderSize | Size of folder string |
| UInt16BE  | ObjectSize | Size of object string |
| UInt8[]  | FolderList | String list of folders `id\tname\n...` |
| UInt8[]  | ObjectList | String list of objects `id\tname\n...` |

### 1008 - ResourceData

This is used as part of the admin interface to list data for datablock objects.

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt16BE  | TableID | ID of object table type |
| UInt8  | NameSize | size of object table name |
| UInt8  | Name[NameSize] | object table name |
| UInt8[RemainingSize]  | Data | String of data `key\tvalue\n...` |

Table category names for objects should be:

| Code | Name |
| ------------- | ------------- |
| 0  |  |
| 1  | textures |
| 2  | shapes |
| 3  | interiors |
| 4  | scene_objects |
| 5  | interior_objects |
| 6  | clothing_objects|
| 7  | particle_objects |
| 8  | light_objects |
| 9  | interactive_objects |
| 10  | tool_objects |
| 11  | pet_objects |
| 12  | travelmount_objects |
| 13  | npc_objects |

OVOPEN ONLY: if category names are postfixed by ".json", resource data is considered to be in JSON format.

## 1001 - RequestCatChildren

This is used as part of the admin interface to enumerate a template category for game objects.

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt32BE  | categoryID | ID of category (0 if root) |
| UInt32BE  | FolderID | size of object table name (used in callback) |
| UInt32BE  | TableID | destination control ID (used in callback) |
| UInt8[RemainingSize]  | Category | Name of object category (see `ResourceData`) |

The server should send back a `CatChildren` packet.

## 1003 - RequestCatMoveObject

This is used as part of the admin interface to move a game object from one template category to another.

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt8  | isFolder | Flags if object to move is folder |
| UInt32BE  | SourceID | Source category ID |
| UInt32BE  | DestID | Destination category ID |
| UInt8[RemainingSize]  | Category | Name of object category (see `ResourceData`) |

## 1004 - RequestAddCategory

This is used as part of the admin interface to add a template category for game objects.

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt32BE  | parentID | Parent category ID (0 if root) |
| UInt8  | CategoryLength | Length of Category |
| UInt8[CategoryLength]  | Category | Name of object category (see `ResourceData`) |
| UInt8[RemainingSize]  | Name | Name of category |


## 1005 - RequestDeleteCategory

This is used as part of the admin interface to delete a template category for game objects.

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt32BE  | categoryID | Category ID |
| UInt8[RemainingSize]  | Name | Name of object category (see `ResourceData`) |


## 1006 - RequestRenameCatObject

This is used as part of the admin interface to rename a template category for game objects.

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt32BE  | srcID | Object ID (0 if root) |
| UInt8  | isFolder | Flag if object is folder |
| UInt8  | CategoryLength | Length of Category |
| UInt8[CategoryLength]  | Category | Name of object category (see `ResourceData`) |
| UInt8[RemainingSize]  | Name | New name of category |

## 1007 - RequestResourceData

This is used as part of the admin interface to query template data for game objects.

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt32BE  | tableID | destination control ID (used in callback) |
| UInt32BE  | ResourceID | ID of Resource Object |
| UInt8[RemainingSize]  | Category | Name of object category (see `ResourceData`) |

The server should send back a `ResourceData` packet.

OVOPEN ONLY: if category names are postfixed by ".json", resource data will be returned in JSON format.

## 1009 - RequestUpdateResource

This is used as part of the admin interface to update template data for game objects.


| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt8  | CategoryLength | Length of object category name |
| UInt32BE  | ResourceID | ID of Resource Object |
| UInt8[CategoryLength]  | Category | Name of object category (see `ResourceData`) |
| UInt8[RemainingSize]  | Data | Data of resource |

OVOPEN ONLY: if category names are postfixed by ".json", resource data is considered to be in JSON format.

## 1010 - RequestDeleteResource
This is used as part of the admin interface to delete template data for game objects.

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt32BE  | categoryID | Category ID |
| UInt8[RemainingSize]  | Name | Name of object category (see `ResourceData`) |

## RPCTransport Types

## 1 - ResolveURL

Implemented in the RPC_ResolveURL class.

This is used to resolve instance ids for urls or location ids. The following extra sub-packets are used:

| Sub-Packet ID | Originator | Name | Comments |
| ------------- | ------------- | ------------- | ------------- |
| 1  | Client | ResolveURL | Instruct the server to resolve a URL |
| 2  | Client | GetInstances | Instruct the server to return instances for a location |
| 3  | Server | ResolvedLocation | Location id for the resolved location |
| Auto(201)  | Server | InstanceList | List of instances for a location |

The following response codes are valid:

| Code | Name |
| ------------- | ------------- |
| 0 | Teleport denied |
| 1 | Invalid house |
| 2 | Invalid location |
| 3 | User offline |
| 4 | Invalid username |
| 5 | URL error |
| 6 | Unknown instance |
| 7 | Unknown location |
| 8 | Bad URL |
| 9 | Invalid URL |
| 10 | Ok |

### Subpacket 1 - ResolveURL

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| String  | name | Name of location |

`name` should be of the form `ONVERSE://location/name`, `ONVERSE://location/name/instance`, or `ONVERSE://location/name/instance/poi`.

The server should respond with a `ResolvedLocation` and `Return` subpacket, or a `Return` subpacket.

### Subpacket 2 - GetInstances

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| Int32BE  | locationID | ID of location |

The server should respond with a `InstanceList` and `Return` subpacket, or a `Return` subpacket.

### Subpacket 3 - ResolvedLocation

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| Int32BE  | locationID | ID of location, -1 if invalid |
| Int32BE  | instanceID | Default Instance number of location, -1 if invalid |
| String  | location | Name of location |
| String  | instance | Name of instance |
| String  | poi | Name of POI |

The client can then use this information to fill in a `RequestInstance` or `ResolveURL.GetInstances` packet. If `instanceID` is -1, the client will opt to show a gui directory for the particular location, otherwise it will submit a `RequestInstance` packet to attempt to connect to the location.

Valid locationID values can be any of the following:

| ID | Name |
| ------------- | ------------- |
| 9 | Hub |
| 10 | Island |
| 14 | paradisefree |
| 18 | metroviewdeluxe |
| 19 | paradisedeluxe |
| 20 | metroviewfree |
| 21 | Tutorial |
| 23 | guide_hall |
| 24 | icefall |
| 25 | ancientmoon |
| 28 | metroview_ph |
| 29 | soldeluxe |
| 30 | solfree |
| 32 | splatball |
| 36 | paradise_estates |
| 37 | sol_estates |
| 38 | echo_canyon |
| 39 | lodges |
| 40 | magical_forest |
| 41 | burning_lands |
| 42 | paradise_land |
| 43 | sol_land |
| 44 | mountainview_land |
| 45 | burning_housing |
| 51 | trials_of_the_ancients |
| 52 | echo_canyon_land |
| 53 | forest_land |
| 54 | forest_estates |
| 55 | tang_coast |
| 56 | metro_ph_deluxe |
| 58 | ancient_cultures |
| 59 | sharktank |
| 60 | Events |
| 61 | suburbia |
| 62 | paradise_megaplots |
| 63 | furniture_shop |
| 64 | candyland |
| 65 | candyland_plots |
| 66 | casino |
| 67 | winter_wonderland |
| 68 | valentines |
| 69 | mardi_gras |
| 70 | spd |

### Subpacket Auto(201) - InstanceList

The response packet starts off with the following:

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| Int32BE  | locationID | ID of location, -1 if invalid |
| UInt32BE  | numInstances | Number of instances |

For each instance:

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |]
| UInt32BE  | instanceID | Instance ID |
| UInt32BE  | numUsers | Users present |
| UInt32BE  | numHomes | Homes present |
| UInt32BE  | numFriends | Friends present |
| String  | name | Name of instance |


## 2 - RequestInstance

Implemented in the RPC_RequestInstance class.

This is used to get a server and port address for an instance. The following extra sub-packets are used:

| Sub-Packet ID | Originator | Name | Comments |
| ------------- | ------------- | ------------- | ------------- |
| 1  | Client | InstanceInfo | Instruct the server to tell us how to connect to an instance |

The server should negotiate with the backend location servers to prepare them for the user to connect. Then `InstanceInfo` packet should then be returned to the client so they can connect to a location.

The following response codes are valid:

| Code | Name |
| ------------- | ------------- |
| 0 | Ok |
| -3 | Unknown instance |
| -4 | Unknown location |
| -5 | Cannot connect |

### Subpacket 1 - InstanceInfo

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| Int32BE  | locationID | ID of location |
| Int32BE  | instanceID | ID of instance |

## 3 - FriendRequest

Implemented in the RPC_FriendRequest class.

This is used to request, accept, ignore, or delete a friend status with another user. The server should send `FriendUpdate` packets to reflect any status changes as a result of the request, and also update any relevant internal friend list for future logins.

| Sub-Packet ID | Originator | Name | Comments |
| ------------- | ------------- | ------------- | ------------- |
| 1  | Client | Request | Process friend request |

The server should return a `Response` packet indicating if the request is valid.

The following response codes are valid:

| Code | Name |
| ------------- | ------------- |
| 0 | Ok |
| -5 | User is ignoring us |
| -4 | Too many friends |
| -3 | Invalid username |

### Subpacket 1 - Request

| Field Type | Name     | Comments      |
| ---------- | -------- | ------------- |
| Uint8      | code     | Request code  |
| String     | username | Username      |

Where `code` is one of the following:

| Code | Name   |
| ---- | ------ |
| 0    | Add    |
| 1    | Accept |
| 2    | Delete |
| 3    | Ignore |

Generally, states should be changed as follows for the requests, subject to other limits and conditions.

| Request | LocalState               | TargetState         |
| ------- | ------------------------ | ------------------- |
| Add     | `Pending friend request` | `Requires approval` |
| Accept  | `Offline`/`Online`       | `Offline`/`Online`  |
| Delete  | `Deleted`                | `Deleted`           |
| Ignore  | `Ignored`                | `Deleted`           |


## 4 - CustomerService

Implemented in the RPC_CustomerService class.

This handles common operations related to customer service, e.g. kicking, banning.

| Sub-Packet ID | Originator | Name | Comments |
| ------------- | ------------- | ------------- | ------------- |
| 1  | Client | Request | The request |

The server should return a `Response` packet indicating if the request is valid.

The following response codes are valid:

| Code | Name |
| ------------- | ------------- |
| 0 | Invalid arg count |
| 1 | Invalid command |
| 2 | User offline |
| 3 | Invalid Username |
| 4 | No permission |
| 5 | Ok |

### Subpacket 1 - Request

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| Uint8  | command | Command code |
| UInt8  | numArgs | Number of args |
| String[numArgs]  | args | Args |

Where `command` is one of the following:

| Code | Name | Args |
| ------------- | ------------- | ------------- |
| 0 | Invalid |  |
| 1 | Kick | Username |
| 2 | KickAll |  |
| 3 | SrvMsg | Username, Message / Message |
| 4 | SrvAnnounce | Message |
| 5 | Suspend | Username, Hours |
| 6 | Ban | Username, 1=Ban, 0=Unban |
| 7 | AddCurrency | Username, CC, PP |

Response code should be 5 if ok, or the relevant code if an error occurs.

## 5 - ServerManagement

Implemented in the RPC_ServerManagement class (internal builds only).

## 6 - StoreFolders

Implemented in the RPC_StoreFolders class (internal builds only).

## 7 - PrivateMessage

Implemented in the RPC_PrivateMessage class.

This handles sending messages.

| Sub-Packet ID | Originator | Name | Comments |
| ------------- | ------------- | ------------- | ------------- |
| 1  | Client | Message | Ask for mount permissions |

The server should return a `Response` packet indicating if the request is valid.

The following response codes are valid:

| Code | Name |
| ------------- | ------------- |
| 0 | Ok |
| -4 | User is offline |
| -3 | Invalid username |

### Subpacket 1 - Message

For `Guides` and `Friends` messages:

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt8  | type | Destination type (2) |
| String  | content | Message |

For everything else:

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt8  | type | Destination user |
| String  | username | Destination user |
| String  | content | Message |

Where `code` is one of the following:

| Code | Name | Destination |
| ------------- | ------------- | ------------- |
| 0 | None | None |
| 1 | Private | To specific user |
| 2 | Friends | To users friends |
| 3 | Guides | To guides |
| 4 | GuideReply | To user as guide (Guides ONLY) |

In the case of guide messages:

* For normal users, upon sending a `Guides` message it should appear to other guides using the `User to guide` message type.
* If a guide sends a guides message, it should appear to other guides as a `Guide` message type.
* If a guide sends a guides message, they should NOT receive a copy of the message.
* If a guide sends a `GuideReply` message, it should appear as a `GuideReplyMessage` to the user. Guides should receive the `GuideReplyMessage` packet with the guides username.

## 8 - InstanceHousing

Implemented in the RPC_InstanceHousing class.

This is used to get a list of houses in an instance

| Sub-Packet ID | Originator | Name | Comments |
| ------------- | ------------- | ------------- | ------------- |
| 1  | Client | Request | Query for housing for instance |
| 2  | Server | HousingList | List of houses |

The server should return either `HousingList` and `Response`, or a `Response` packet.

The following response codes are valid:

| Code | Name |
| ------------- | ------------- |
| 0 | Ok |
| -3 | Error |

### Subpacket 1 - Request

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| Int32BE | locationID | ID of location |
| Int32BE | instanceID | ID of instance |

### Subpacket 2 - HousingList

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| Uint8  | numHouses | Number of houses |

For each house:

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| String  | poiName | POI name of house (should match map screen id) |
| String  | name | Users name of house |
| String  | description | Description of house |
| Uint32BE | CC | CC cost |
| Uint32BE | PP | PP cost |
| Uint32BE | maxFurniture | Maximum furniture |
| Uint8  | iconID | Icon ID |
| Uint32BE | ownerID | Owner account |
| String  | username | Username |
| Uint8  | isFriend | Bool if house belongs to a friend |

## 9 - GetUserHousing

Implemented in the RPC_GetUserHousing class.

This is used to get a list of a users houses.

| Sub-Packet ID | Originator | Name | Comments |
| ------------- | ------------- | ------------- | ------------- |
| 1  | Client | Request | Query for housing for user |
| 2  | Server | HousingList | List of houses |

The server should return either `HousingList` and `Response`, or a `Response` packet.

The following response codes are valid:

| Code | Name |
| ------------- | ------------- |
| 0 | Ok |
| -3 | Invalid username |
| -4 | Denied |
| -5 | No house |

### Subpacket 1 - Request

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| String  | username | Name of user |

### Subpacket 2 - HousingList

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| Uint8  | numHouses | Number of houses |

For each house:

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| Uint32BE | deedID | ID of house plot |
| Uint32BE | CC | CC cost |
| Uint32BE | PP | PP cost |
| Uint8  | iconID | Icon ID |
| String  | locationName | Location name |
| String  | instanceName | Instance name |
| String  | poiName | POI name |
| String  | homeName | Home name |

## 10 - EditUserHouse

Implemented in the RPC_EditUserHouse class.

This is used to edit base properties of a users house plot

| Sub-Packet ID | Originator | Name | Comments |
| ------------- | ------------- | ------------- | ------------- |
| 1  | Client | Rename | Set house name |
| 2  | Client | MakeDefault | Make house default |

The server should just return a `Response` packet.

The following response codes are valid:

| Code | Name |
| ------------- | ------------- |
| 0 | Ok |

### Subpacket 1 - Rename

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt32BE | deedID | ID of home to rename |
| String  | name | New name for home |

### Subpacket 2 - MakeDefault

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| Uint32BE | deedID | ID of home to set as default |

## 11 - UserPermissions

Implemented in the RPC_UserPermissions class.

This is used to set the mount permissions on a user.

| Sub-Packet ID | Originator | Name | Comments |
| ------------- | ------------- | ------------- | ------------- |
| 1  | Client | GetPermissionInfo | Ask for mount permissions |
| 2  | Server | PermissionInfo | Permission info |
| 3  | Client | SetPermissionInfo | Set mount permissions |

The server should return `PermissionInfo` for the `GetPermissionInfo` packet, otherwise returning just a `Response` is valid.

The following response codes are valid:

| Code | Name |
| ------------- | ------------- |
| 0 | Ok |

The following permissions values are valid:

| Code | Name |
| ------------- | ------------- |
| -1 | NO CHANGE (i.e. don't set it) |
| 0 | Public |
| 1 | Friends |
| 2 | Private |

### Subpacket 1 - GetPermissionInfo

No extra data.

### Subpacket 2 - PermissionInfo

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| Int8  | teleportPerms | Permissions for teleport |
| Int8  | homePerms | Permissions for home |

### Subpacket 3 - SetPermissionInfo

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| Int8  | teleportPerms | Permissions for teleport |
| Int8  | homePerms | Permissions for home |
