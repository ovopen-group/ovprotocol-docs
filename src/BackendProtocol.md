# Backend Protocol

This page describes the backend server protocol, which works on top of the general protocol. This is the protocol used by the location servers to talk with the master server.

In all cases, you will need to authenticate with `RegisterLocationServer` before performing any other backend RPC requests is permitted.

NOTE: This is only a suggested protocol for ovopen. This does not reflect the original protocol used.

## Auto packet Types

All of these packets are sent and received without being encapsulated in any other packet. All of these packets are auto packets.

### 500 - UserLoginQueued

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt32BE  | UID | User ID |
| UInt32BE  | PermissionsMask | User Permissions |
| UInt32BE  | LastInventoryID | Last Inventory ID |
| UInt8  | IsVIP | VIP flag |
| UInt8  | AvatarSex | Avatar Sex (0 = male, 1 = female) |
| Uint8[20]  | authHash | authorization hash |
| String  | Username | Username |

This allows the location server to login the specified user. A blank authorization hash should clear details for the user, and should disconnect them if they are present.

### 501 - UserShouldBeKicked

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt32BE  | UID | User ID |

This tells the server a user should be kicked. Usually happens when they join another location.

### 502 - InventoryData

Contains the inventory for a user. The server needs to update its version of the inventory.

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt32BE  | UID | User ID |
| UInt32BE  | TypeID | Type of inventory |
| char[]  | Data | Data |

### 503 - HousePlot

Lists furniture and ownership info for a house plot. This will be sent upon registration and whenever a house plot is updated through some process other than on the location server.

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt32BE  | DeedID | Deed ID |
| UInt32BE  | PlotID | Local Plot ID |
| UInt32BE  | OwnerID | UID of owner |
| UInt32BE  | TypeID | TypeID |
| UInt32BE  | MaxFurniture | Max furniture |
| UInt32BE  | CC | CC Cost |
| UInt32BE  | PP | PP Cost |
| String  | Name | Name |
| char[]  | Data | Object data |

### 504 - UserInventoryId

Updates last inventory id

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt32BE  | UID | User ID |
| UInt32BE  | LastID | Last Inventory ID |

### 505 - ProcesssManagement

Tells the server if it needs to shutdown.

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt32BE  | Code | Code |
| UInt32BE  | Param | Param |

Code can be one of the following:

| Code | Name |
| ------------- | ------------- |
| 0 | NOP |
| 1 | Terminate |
| 2 | KickEveryone |
| 3 | ReloadUser (UID) |

### 506 - ServerInfo

Tells the server the latest registered location of the server.

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| String  | Server URL | URL to connect to server from client |
| Int32BE  | LocationID | Location ID |
| Int32BE  | InstanceID | Instance ID |

### 507 - UserBalance

Tells a location server what the users new CC and PP balance is. May be sent any time

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt32BE  | UserID | UID |
| Int32BE  | CC | Users CC |
| Int32BE  | PP | Users PP |

###

## TorqueRPC Types

## 9001 - RegisterLocationServer

The location server will send this to let the master server identify how clients can connect to this location server.

Client should get back a `ServerInfo` packet.

| Sub-Packet ID | Originator | Name | Comments |
| ------------- | ------------- | ------------- | ------------- |
| 1  | Client | RegistrationInfo | Registration Info |

Return codes (packet 0)

| Code | Name |
| ------------- | ------------- |
| -2 | Registration unsuccessful |
| 0 | Registration successful |
| 1 | Not registered |

### Subpacket 1 - RegistrationInfo

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt32  | InstanceID | ID of instance record on server |
| UInt16  | PortID | Port server is listening on |

## 9002 - LocationReadyState

Tell the location server our current state - i.e. can people login and the server should be advertised. Should be set after a successful `SyncLocationState`.

| Sub-Packet ID | Originator | Name | Comments |
| ------------- | ------------- | ------------- | ------------- |
| 1  | Client | StateInfo | State Info |

Return codes (packet 0)

| Code | Name |
| ------------- | ------------- |
| -2 | Error occured |
| 0 | Success |

### Subpacket 1 - StateInfo

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt32BE  | PermissionsMask | Mask of users allowed here |
| bool  | vipOnly | VIPs only |

## 9003 - RequestInventory

The location server should call this during the login process of the user to get the latest state of the users inventory, for each object type needed. The master server will then send `InventoryData` packets, then complete the RPC request with 0, or an error if anything went wrong.

| Sub-Packet ID | Originator | Name | Comments |
| ------------- | ------------- | ------------- | ------------- |
| 1  | Client | InventoryInfo | Inventory Info |
| 2  | Client | InventoryID | Get Inventory ID |

Return codes (packet 0)

| Code | Name |
| ------------- | ------------- |
| -2 | Error occurred |
| 0 | Success |

### Subpacket 1 - InventoryInfo

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt32BE  | UID | User ID |
| UInt32BE  | ObjectType | Type ID |

### Subpacket 2 - InventoryID

Server should send a `UserInventoryId` packet containing the last inventory id of the user.

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt32BE  | UID | User ID |

# 9004 - NotifyTransaction

Tells the master server to register a transaction relating to an inventory object. The server will then send back a transaction id in the `TransactionID` packet.

Transaction data should be sufficient to reconstruct user balance and inventory items. The server may also send back a new CC and PP amount for the user, in the form of a `UserBalance` packet.

| Sub-Packet ID | Originator | Name | Comments |
| ------------- | ------------- | ------------- | ------------- |
| 1  | Client | TransactionInfo | Transaction ID |
| 2  | Server | TransactionID | Transaction ID |
| 3  | Client | QueryBalance | Query balance (should return a `UserBalance` packet) |

Return codes (packet 0)

| Code | Name |
| ------------- | ------------- |
| -2 | Error occured |
| 0 | Success |

### Subpacket 1 - TransactionInfo

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt32BE  | UID | User ID |
| UInt32BE  | ObjectType | OBJ ID |
| UInt32BE  | ActionID | Integer reflecting action type |
| Int32BE  | CCDiff | Balance diff of CC |
| Int32BE  | PPDiff | Balance diff of PP |
| UInt32BE  | ServerID | Server ID |

### Subpacket 2 - TransactionID

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UCHAR[8]  | TransactionID | Transaction ID |

### Subpacket 3 - QueryBalance

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt32BE  | UID | User ID |

Should send back a `UserBalance` packet.

# 9005 - CommitInventory

Tells the master server to update a clients inventory. Inventory can be added, removed, or reset to a known state.

| Sub-Packet ID | Originator | Name | Comments |
| ------------- | ------------- | ------------- | ------------- |
| 1  | Client | ReplaceInventory | Replace inventory |
| 2  | Client | AddInventory | Add inventory items |
| 3  | Client | RemoveInventory | Remove inventory items |
| 4  | Client | UpdateInventoryID | Updates last inventory ID for user |

Return codes (packet 0)

| Code | Name |
| ------------- | ------------- |
| -2 | Error occured |
| 0 | Success |

All packet types with the exception of `UpdateInventoryID` are of the format:

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt32BE  | UID | User ID |
| UInt32BE  | ObjectType | OBJ Type |
| bool  | ThreePart | Indicates whether a name field is present (in which case a small string is added onto the end) |
| String[packet size]  | InventoryList | List of IDS `item_id\tdb_id\n...` |

In the case of `RemoveInventory`, `InventoryList` need only contain a list of `item_id` records.

`UpdateInventoryID` is of the format:

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt32BE  | UID | User ID |
| UInt32BE  | InventoryID | Last Inventory ID |

# 9006 - ExpendUserLoginQueue

Tells the master server to that we are ready to accept connections from the user. NotifyUserReady RPC packets should be used to clear the queue, IN THE ORDER THE `UserLoginDetails` ARE SUPPLIED. 1 request should be issued per user.

| Sub-Packet ID | Originator | Name | Comments |
| ------------- | ------------- | ------------- | ------------- |
| 1  | Client | UserInfo | User Info |

Return codes (packet 0)

| Code | Name |
| ------------- | ------------- |
| -2 | Error occured |
| 0 | Success |

### Subpacket 1 - UserInfo

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt32BE  | UID | User ID |
| UInt8  | AcceptType | Accept type |

`AcceptType` may be the following values:

| Code | Name | Destination |
| ------------- | ------------- | ------------- |
| 0 | Not accepted | User should not be allowed to login |
| 1 | Accepted | User is allowed to login |


# 9007 - NotifyUserPresence

Tells the master server if the user is present on the server.

| Sub-Packet ID | Originator | Name | Comments |
| ------------- | ------------- | ------------- | ------------- |
| 1  | Client | UserInfo | User Info |

Return codes (packet 0)

| Code | Name |
| ------------- | ------------- |
| -2 | Error occured |
| 0 | Success |

### Subpacket 1 - UserInfo

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt32BE  | UID | User ID |
| UInt8  | PresenceState | Presence state |

`PresenceState` may be the following values:

| Code | Name | Destination |
| ------------- | ------------- | ------------- |
| 0 | Not present | UID is not present |
| 1 | Present | UID is present |

# 9008 - CommitHousePlot

Tells the master server to update a house plot.

| Sub-Packet ID | Originator | Name | Comments |
| ------------- | ------------- | ------------- | ------------- |
| 1  | Client | PlotInfo | Plot Info |
| 2  | Server | PlotIdentifier | Plot Identifier |

Return codes (packet 0)

| Code | Name |
| ------------- | ------------- |
| -2 | Error occurred |
| 0 | Success |

### Subpacket 1 - PlotInfo

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt32BE  | OwnerID | UID of owner |
| UInt32BE  | PlotID | Local Plot ID |
| String  | Data | Object data |

### Subpacket 2 - PlotIdentifier

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt32BE  | GID | GID of plot |


