# Basic Protocol

This page describes the general behavior of the onverse master server protocols. Several protocols are built upon this basic foundation.

## Packet Format

All packets should have a header of the following basic format, followed by the payload. Any packet which does not conform to this basic format should cause a connection error.

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt16BE  | Code | To be valid `(Code & 0xffd6) == 0xffd6` must be true |
| UInt16BE  | PacketSize  | Size of packet excluding header size (6)  |
| UInt16BE  | NativeID | Packet identifier  |

The `Code` field also has the following flags

| Flag | Name | Comments |
| ------------- | ------------- | ------------- |
| 0x1  | Compressed | If true, packet data is compressed |
| 0x8  | Encrypted  | If true, packet data is encrypted  |
| 0x20  | AutoPacket | If true, packet is an "auto" packet |

If a packet is `Encrypted`, the packet data is encrypted according to the [Encrypted Packets](#EncryptedPackets) section.  Similarly, if a packet is `Compressed` it is compressed according to the [Compressed Packets](#CompressedPackets) section.

If a packet is both `Encrypted` and `Compressed`, compression occurs before the packet is encrypted. To decode, the packet must be decrypted first. To encode, the packet must be compressed first.

Generally there are two distinct types of packets each with different handlers.

* AutoPackets, which use a class-based handler system which can easily be changed by the implementation
* Basic packets, which use hardcoded conditional statements

There is only 5 types of auto packets:
* `RPCTransport` which wraps the RPC communication protocol in the MasterProtocol.
* `RequestAuth` which requests the client start authenticating.
* `Forward` which redirect the client to a new server.
* `LocationInstanceList` which lists instances for a location (though is the only one of these packets always wrapped in an RPC transport)

All packet ids are unique across all users of the basic protocol, so it should be possible to write a client or server that handles every single case. However, servers should be implemented in such a way that packets are accepted only when the connection type permits it. e.g. if we connect to a patcher server, it shouldn't accept any `ResolveURL` packets.

Also it should be noted that until the connection has been authenticated, the server should only respond to `Pong` and `Login` packets.

## Compressed Packets

Packet data should be compressed with zlib, using the default `deflateInit` with compression level 9.

At most, `PacketSize` bytes should be fed in, while the decompressed size of the stream is restricted by whatever can be encoded into the packet. Thus the true size of the packet data can exceed 65536 bytes. However in practice the client or server should restrict the amount of compressed bytes to avoid excessive memory usage.

## Strings

In most cases, the `String` will be stored using the following format:

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt8  | Code | Should be `0xea` |
| UInt16BE  | Length | Length of string |
| UInt8[Length]  | Data | String data, without terminating `0` |

In a few cases, the packet size will be used to determine the length of the string. These will be noted as required.

## Encrypted Packets

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt8[]  | Data | Encrypted data |
| UInt8[]  | Padding | Extra encrypted data padded to the next 8 bytes |
| UInt32BE | Signature  | `adler32` signature of the decrypted data |

For encrypted packets, the true size of the packet is increased by 4 bytes, then aligned to the next 8 bytes. e.g. `((PacketSize+4) + 0x7) & ~0x7`

Any extra bytes used to pad for alignment should be either blank or random. The last 4 bytes of the packet should be the `adler32` signature of the decrypted `Data` (excluding padding), the iv being `adler32(0,0,0)`.

The algorithm used for encryption should be blowfish in ecb mode. The encryption key should be set as part of the login process.

For best results, use the following functions from openssl:

* `BF_set_key` to set the key
* `BF_ecb_encrypt` in chunks of 8 bytes with `BF_DECRYPT` or `BF_ENCRYPT` set

Also use `adler32` from zlib.

## Compression vs encryption

Technically all packets can be independently compressed and encrypted. However in the case of certain packets such as the RPC handler, the outer packet should never be compressed.

## Basic Packets

All servers should at least implement the following packets to facilitate login and thus allow for encrypted packets.

All packet numbers are expressed as their decoded value in decimal. None of these packets should have `AutoPacket` set.

### 1 - ServerError

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt32BE  | Code | Error code |

This should be sent anytime an error occurs which will cause the connection to terminate, such as when logging in.

| Code | Comments |
| ------------- | ------------- |
| 0  | Unknown |
| 1  | General error |
| 2  | Bad packet |
| 3  | Packet build error |
| 4  | Authentication failure |
| 5  | MySQL error |
| 6  | Instance list incorrect |
| 7  | Invalid request argument |
| 8  | Kicked |

### 2 - Ping

This is sent to by the server every 30 seconds after the server has last received a packet. The client must then sent a `Pong` within at least 30 seconds to maintain the connection.

### 3 - Pong

This is sent by the client as a response to `Ping`.

### 102 - Login

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt8[32]  | Username | Username |
| UInt8[20]  | HashCode | Computed hash code |

This packet should be sent as a response to `AuthCode` by the client.

### 103 - AuthSuccess

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt32BE | UID | id of user |
| Uint32BE  | PermissionsMask | Permissions mask |

This packet should be sent upon a successful login. The permissions mask is a set of bit flags described as follows:

| Bit | Name | Comments |
| ------------- | ------------- | ------------- |
| 1  | USER | |
| 2  | GUIDE  |  |
| 3  | SUPERGUIDE |  |
| 4  | COMM_MANAGER |  |
| 5  | ADMIN |  |
| 6  | SUPER_ADMIN |  |
| 7  | NETWORK_ADMIN |  |
| 8  | ROOT_ADMIN |  |
| 9  | DEVELOPER | |
| 32  | SUPER_DEV |  |

### 104 - AuthFailure

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt32BE  | Code | Failure code |

This packet should be sent as a response to `AuthCode` by the client, before `ServerError`. Failure codes are handled in script as follows:

| Code | Comments |
| ------------- | ------------- |
| 2  | Bad username or password |
| 3  | Connection is denied |
| 4  | Account is banned |
| 5  | Account is suspended |

Otherwise the failure is unknown

### 205 - MasterVersion

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt8[PacketSize]  | Banner | Description of server |

This packet should be sent upon a successful login.

## AutoPacket types

### 101 - RequestAuth

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt8[8]  | SecurityCode | Security code |

This packet should be sent by the server as soon as the connection has been established. It is used to begin authentication as described in (Authentication)[Authentication].

### 5 - Forward (post-2012)

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt8[4]  | address | IP address of server  |

This packet should be sent by the server to redirect the client to a new server.

### 10001 - RPCTransport

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| UInt16BE  | rpcID | RPC identifier |
| UInt32BE  | requestID | Request identifier  |
| BasicPacket  | data | wrapped data  |

Every new RPC request should increment `requestID` by 1. The actual number should then be shifted to the left by 16 bits before being stored in the packet.

For client->server RPC requests, requestID is calculated using:

```
  counter = ((counter & 0xffff)+1) & 0xffff)
  requestID = counter << 16
```

For server->client RPC requests, requestID is calculated using:

```
  counter = ((counter & 0xffff)+1) & 0xffff)
  requestID = counter
```

i.e. client RPC requests store their values in the upper 16 bits, while server RPC requests store their values in the lower 16 bits. This allows for client-initiated and server-initiated RPC requests to be differentiated from one another.

Each RPC request should be wrapped in a `RPCTransport` packet. The client will keep track of all pending RPC requests until either the server returns a responding packet, or the connection disconnects.

Most RPC requests will wrap basic packets in order to provide request and response data. In the simplest of cases, the RPC packet will just have a 32bit response code in the wrapped packet. In the more advanced cases large blobs of encrypted, compressed data will be present.

#### Subpacket 0 - Return

This packet is used on all RPC request types to indicate a request has been completed. All RPC requests should be terminated using this packet.

| Field Type | Name | Comments |
| ------------- | ------------- | ------------- |
| Int32BE  | code | Response code |

Response codes are different between each RPC request type, but 0 almost always means success. Usually upon termination, the client will terminate pending RPC requests using code -2, so any additional error codes either start from -3, or 1.

## Authentication

The server should begin by sending the `AuthCode` packet. This contains a randomized 8 byte security code.

The client should then send a `Login` packet with a username and `HashCode`. The code should be generated by concatenating the SHA1 hashed equivalent of the users password with the `SecurityCode` bytes received from the server. i.e. `SHA1(SHA1(password) + SecurityCode)`

The server should compare this client-generated hash with its equivalent computed value and return either:

* `MasterVersion`, `AuthSuccess` if the authentication was successful
* `AuthFailure`, `ServerError` if the authentication failed

If the authentication was successful, the client should also generate an encryption key to use for encrypted packets. This can be done by calculating `SHA1(SecurityCode + SHA1(password) + "%^%zre$%^")` (the string including the terminating NULL). Usually encrypted packets should be used for most communication besides packets that can be shared between multiple clients (such as group chats, announcements, and pings). However this is not a strict requirement.
