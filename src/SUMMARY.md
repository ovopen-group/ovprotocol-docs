# Summary

- [Basic Protocol](./ProtocolDocs.md)
- [Master Protocol](./MasterProtocol.md)
- [Patcher Protocol](./PatcherProtocol.md)
- [Patch File Format](./PatchFileFormat.md)
- [Backend Protocol](./BackendProtocol.md)

- [License](./License.md)